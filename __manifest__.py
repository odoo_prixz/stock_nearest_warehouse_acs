# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Stock Nearest Warehouse',
    'category': 'Warehouse',
    'version': '1.0',
    'summary': """
        Auto find nearest warehouse from customer zip.
    Stock Nearest Warehouse
Stock Nearest Warehouse
nearest warehouse
warehouse
inventory
stock
sales
sales management
invoicing
pick stock
manage warehouse
postal codes
zip codes
customer zip code
customer postal code
manage warehouse with zip code
manage warehouse with postal code
lot number
serial number
product tracking
choose warehouse
auto pickup stock
stock expiry
product expiry date
expiry date
product dead alert
warehouse product
item expiry
item expiry date
item expiry date alert
stock expiry report
item expiry report
product expiry report
product expriy elert
Stock Ageing Analysis
ageing
analysis
ageing analysis
stock analysis
stock ageing
value
stock value
ageing stock value
location
stock location
stock location value
product ageing analysis
ageing product
product ageing value
warehouse product ageing value
warehouse product ageing analysis
inventory value
inventory ageing
inventory ageing value
inventory ageing value analysis
stock ageing excel report
excel report
inventory ageing report
product ageing excel report
product ageing report
stock ageing report
stock ageing excel
payment
estimation
stock in hand
age of stock in hand
age vise breakup of Inventory
old stock
ageing slabs
depreciate
obsolete
Ageing Analysis Report
duration
quantity
Stock Journals
ageing period
Valuation Method
FIFO
drill-down
transaction voucher
purchase
inventory age report
inventory coverage report
products report
product report
break down report
Pallet Storage
Warehouse Storage
Warehousing Company
Storage Company
Pallet Storage Company
Logistics
Warehousing
Storage
De-containerisation
Steel Partitioning
Industrial Partitions
Warehouse Partitioning
Single Skin Steel Partitioning
Double Skin Steel Partitioning
Transport
Haulage
Pallet Distribution
New Forklifts
Used Forklifts
Forklift Repiars
Forklift Parts
Forklift Training
HMRC Bonded Warehousing
General Warehousing
Customs Warehousing
Ecommerce Fulfilment
Pick And Pack
Warehouse Storage And Distribution
Food And Drinks
Food And Drinks Wholesale
Wholesale For Restaurants
Warehouse Solutions
Wms
Stock Control Software
Warehouse Software
Racking Inspection
Racking Inspections
Rack Inspection
SEMA Racking Inspection
Rack Safety Training
DISTRIBUTION
BOND
Commercial Property
Pallet
Pallet Racking
Office Furniture
Dexion South Yorkshire
Mezzanine Floors
Fulfilment Services
Freight Forwarding
Fulfilment
Data Capture
Account Management
Document Archiving
Surplus
Ex Catalogue
Wholesale
Returns
End Of Line
Low Load Felixstowe
Warehouse Felixstowe
Artic Transport Felixstowe
Hiab Deliveries
Rigid Deliveries
Moving Service
Storage Service
Self-Storage Facility
Record Storage Facility
Removals
Packaging
Distribution (dhd)
Warehouses
Warehouse Space
Pick , Pack
Warehouses To Rent
Buy Solar Window Film
Window Films And Window Tints
Glass Film And Film For Windows
Shopfitting
Office Refurbishment
Industrial Shelving
Retail Shelving
Industrial Racking
Warehouse Management Software
Barcode Printers
RFID
Warehouse Design
Warehousing And Distribution
Road Haulage
Rail Freight Services
COMAH - Chemical Storage
Candy Buffet
Candy Centrepiece
Crystal Wedding Tree
Chocolate Fountain
Wedding Favours
Stock Control
Container Services
Rework
Warehousing And Storage
Pick, Pack And Dispatch
Stocktaking
Warehouse Management
Outsourced Store Keeping
Relocation
Fulfillment House
Pick And Pack Solutions
Ecommerce
Mail Order Fulfilment
Stock Management
Express Delivery Of Palletised Goods
Secure
Reliable
Containers Unloading/reloading
Courier
Pallets
Barcodes
Labels
Signage
Aisle Markers
Archive Management
Secure Shredding
Delivery
Picking
Self Storage
Cheap Storage
Affordable Storage
Lift Truck Training
Fork Lift Training
Counterbalance
Reach
Forklift
Forktruck
Forklifts
Pharmaceutical
Cosmetics
Nutrition
Warwick Warehouse And Logistics
Palletised Consignments And Rackings
Commercial Warehousing
Wedding Cakes
Birthday Cakes
Theme Parties
Cakes
Bakery Goods

""",
    'author': 'Aurayan Consulting Services',
    'website': '',
    'description': """
        Auto find nearest warehouse from customer zip.
    """,
    'depends': ['stock', 'sale_stock', 'sale_management'],
    'data': [
        'security/ir.model.access.csv',
        'views/sale_order_views.xml',
        'views/stock_warehouse_views.xml',
        'views/res_config_settings_views.xml',
    ],
    'demo': [],
    'qweb': [],
    'images': [
        'static/description/main_screen.png'
    ],
    'price': 100,
    'currency': 'EUR',
    'installable': True,
    'auto_install': False,
    'application': True,
    'license': 'OPL-1',
}
