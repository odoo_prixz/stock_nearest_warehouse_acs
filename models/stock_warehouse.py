# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api, _
from odoo.exceptions import ValidationError


class StockWarehouse(models.Model):
    _inherit = "stock.warehouse"

    zip_range_ids = fields.One2many('stock.zip.range', 'warehouse_id', string="Zip Code", copy=False)
    nearest_ids = fields.One2many('nearest.warehouse',
        'warehouse_id', string='Nearest Warehouses', copy=False)
    is_order_split = fields.Boolean(string="Split de orden")
    zip_state_ids = fields.One2many('stock.state.range', 'warehouse_id', string="Estados cercanos", copy=False)

    @api.model
    def name_search(self, name, args, operator='ilike', limit=100):
        """
        Override method for apply report domain.
        """
        context = dict(self._context)
        if context.get('warehouse_id') and context.get('warehouse_id'):
            args.append(('id', '!=', context['warehouse_id']))
        return super(StockWarehouse, self).name_search(name, args=args, operator=operator, limit=limit)


class StockZipRange(models.Model):
    _name = "stock.zip.range"
    _description = "Zip Range"

    zip_to = fields.Char('To Zip', copy=False)
    zip_from = fields.Char('From Zip', copy=False)
    warehouse_id = fields.Many2one('stock.warehouse', string='Warehouse', copy=False, ondelete='cascade')
    company_id = fields.Many2one('res.company', related='warehouse_id.company_id', copy=False)
    kms = fields.Float('Distancia kms', copy=False)
    priority = fields.Float('Prioridad', copy=False)

    @api.constrains('zip_to', 'zip_from')
    def _check_zip_code(self):
        for rec in self:
            if rec.zip_to and not rec.zip_to.isdigit():
                raise ValidationError(_('Invalid Zip To, \nPlease enter a valid zip to'))
            if rec.zip_from and not rec.zip_from.isdigit():
                raise ValidationError(_('Invalid Zip From, \nPlease enter a valid zip from'))
            zip_ids = self.search([
                    ('warehouse_id', '=', rec.warehouse_id.id),
                    ('id', '<>', rec.id)
                ]).filtered(lambda x: x.zip_from <= rec.zip_from <= x.zip_to or \
                                    x.zip_from <= rec.zip_to <= x.zip_to)
            if zip_ids:
                raise ValidationError(_("You can not have an overlap between two zip range, \n\
                                    Please correct the zip from and/or zip to of your zip range."))


class NearestWarehouse(models.Model):
    _name = "nearest.warehouse"
    _description = "Nearest Warehouse"
    _order="sequence asc"

    warehouse_id = fields.Many2one('stock.warehouse', string="Warehouse", copy=False)
    nearest_warehouse_id = fields.Many2one('stock.warehouse', string="Nearest Warehouse", copy=False)
    sequence = fields.Integer(index=True, copy=False,
        help="Gives the sequence order when displaying a list of Nearest Warehouse.")

    _sql_constraints = [
        ('nearest_warehouse_id_uniq', 'unique(warehouse_id, nearest_warehouse_id)', 'The Nearest warehouse must be unique!'),
    ]

class StockStateRange(models.Model):
    _name = "stock.state.range"
    _description = "Zip Range"

    state_id = fields.Many2one('res.country.state', string='Estado',)
    priority = fields.Float('Prioridad', copy=False)
    company_id = fields.Many2one('res.company', related='warehouse_id.company_id', copy=False)
    warehouse_id = fields.Many2one('stock.warehouse', string='Warehouse', copy=False, ondelete='cascade')