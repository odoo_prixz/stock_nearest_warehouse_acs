# -*- coding: utf-8 -*-
# Part of Odoo. See COPYRIGHT & LICENSE files for full copyright and licensing details.

from . import stock_warehouse
from . import sale
from . import res_config_settings
from . import stock_move

