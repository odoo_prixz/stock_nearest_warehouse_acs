# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class ResConfigSettings(models.TransientModel):

    _inherit = 'res.config.settings'

    crum_is_activate = fields.Boolean(
        string="A un solo CRUM",
        config_parameter='nearest.default_crum_is_activate',
        default=False
    )

