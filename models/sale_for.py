# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api, _


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    product_type = fields.Selection(related='product_id.type', string="Product Type")
    sin_stock_warehouse_id = fields.Many2one(
        'stock.warehouse', string='CRUM más cercano',
        readonly=True
    )
    sin_cambio_warehouse_id = fields.Many2one(
        'stock.warehouse', string='CRUM original',
        readonly=True
    )

    @api.model
    def _default_warehouse_id(self):
        return self.env['stock.warehouse'].search([('company_id', '=', self.env.user.company_id.id)], limit=1)

    line_warehouse_id = fields.Many2one(
        'stock.warehouse', string='Line Warehouse',
        readonly=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)]})

    def set_warehouse(self):
        """
            Set best warehouse
        """
        for rec in self:
            rec.line_warehouse_id = False
            if rec.product_id and rec.product_id.type in ['product', 'consu'] and rec.state in ['draft', 'sent']:
                warehouse = rec.get_warehouses(partner_shipping_id=rec.order_id.partner_shipping_id)
                if warehouse:
                    rec.line_warehouse_id = warehouse.id
                elif rec.order_id and rec.order_id.warehouse_id:
                    rec.line_warehouse_id = rec.order_id.warehouse_id.id
                else:
                    rec.line_warehouse_id = rec._default_warehouse_id().id


    @api.model_create_multi
    def create(self, vals):
        res = super(SaleOrderLine, self).create(vals)
        for order in res:
            if order.product_id and order.product_id.type in ['product', 'consu']:
                order.line_warehouse_id = order.order_id.warehouse_id.id
            if order.order_id:
                order.set_warehouse()
                order.sin_cambio_warehouse_id = order.line_warehouse_id.id
        return res

    @api.onchange('product_id', 'product_uom_qty')
    def product_id_change(self):
        res = super(SaleOrderLine, self).product_id_change()
        for rec in self:
            if rec.order_id:
                rec.set_warehouse()
        return res

    def get_warehouses(self, partner_shipping_id):
        """
            This Method return available product warehouse.
        """
        warehouse = self.get_default_warehouse(partner_shipping_id=partner_shipping_id)
        if warehouse:
            return warehouse
        if self.order_id and self.order_id.warehouse_id:
            return self.order_id.warehouse_id
        return self._default_warehouse_id()

    def get_default_warehouse(self, partner_shipping_id):
        """
            This Method return available product wise warehouse form zip to and zip from
        """
        if partner_shipping_id and partner_shipping_id.zip:
            warehouse_id = False
            nearest_warehouse_id = False
            partner_zip = partner_shipping_id.zip
            zip_ids = self.env['stock.zip.range'].search([('zip_from', '!=', False), ('zip_to', '!=', False), ('company_id', '=', self.company_id.id or self.env.context.get('company_id'))]).filtered(lambda l: l.zip_from <= partner_zip.split('-')[0] <= l.zip_to)
            zip_ids = zip_ids.sorted(lambda x: x.priority)
            def _check_zip_range_warehouse(zip_ids):
                for zip_id in zip_ids:
                    available_qty = self.env['stock.quant']._get_available_quantity(self.product_id, zip_id.warehouse_id.lot_stock_id, lot_id=False, strict=False)
                    zip_return = zip_id.warehouse_id if available_qty and available_qty >= self.product_uom_qty else False
                    if not self.sin_stock_warehouse_id:
                        self.sin_stock_warehouse_id = zip_id.warehouse_id
                    if zip_return:
                        return zip_return
                return False
            def _get_nearest_wh(zip_ids):
                for zip_id in zip_ids:
                    return self.get_nearest_warehouse(partner_shipping_id=partner_shipping_id, warehouse_id=zip_id.warehouse_id)
            def return_warehouse(zip_ids):
                warehouse_id = _check_zip_range_warehouse(zip_ids)
                if warehouse_id:
                    return warehouse_id
                else:
                    nearest_warehouse_id = _get_nearest_wh(zip_ids)
                    if nearest_warehouse_id:
                        return nearest_warehouse_id
            if zip_ids:
                wh_id = return_warehouse(zip_ids)
                if wh_id:
                    return wh_id
            if not warehouse_id and partner_zip.split('-') and len(partner_zip.split('-')) == 2:
                partner_zip = partner_zip.split('-')[1]
                zip_ids = self.env['stock.zip.range'].search([('zip_from', '!=', False), ('zip_to', '!=', False), ('company_id', '=', self.company_id.id)]).filtered(lambda l: l.zip_from <= partner_zip <= l.zip_to)
                if zip_ids:
                    wh_id = return_warehouse(zip_ids)
                    if wh_id:
                        return wh_id
        return False

    def get_nearest_warehouse(self, partner_shipping_id, warehouse_id):
        """
            This Method return available product wise Nearest warehouse.
        """
        for nearest_id in warehouse_id.nearest_ids:
            available_qty = self.env['stock.quant']._get_available_quantity(self.product_id, nearest_id.nearest_warehouse_id.view_location_id, lot_id=False, strict=False)
            if available_qty and available_qty >= self.product_uom_qty:
                return nearest_id.nearest_warehouse_id
        return False

    def _prepare_procurement_values(self, group_id=False):
        """ Prepare specific key for moves or other components that will be created from a stock rule
        comming from a sale order line. This method could be override in order to add other custom key that could
        be used in move/po creation.
        """
        values = super(SaleOrderLine, self)._prepare_procurement_values(group_id)
        self.ensure_one()
        warehouse = self.get_warehouses(partner_shipping_id=self.order_id.partner_shipping_id)
        if self.product_id and self.product_id.type in ['product', 'consu']:
            values.update({
                'line_warehouse_id': self.line_warehouse_id or False,
                'warehouse_id': self.line_warehouse_id or self.order_id.warehouse_id or False,
            })
        return values

    @api.depends('product_id', 'route_id', 'order_id.warehouse_id', 'product_id.route_ids', 'line_warehouse_id')
    def _compute_is_mto(self):
        """ Verify the route of the product based on the warehouse
            set 'is_available' at True if the product availibility in stock does
            not need to be verified, which is the case in MTO, Cross-Dock or Drop-Shipping
        """
        self.is_mto = False
        for line in self:
            if not line.display_qty_widget:
                continue
            product = line.product_id
            product_routes = line.route_id or (product.route_ids + product.categ_id.total_route_ids)

            # Check MTO
            mto_route = line.line_warehouse_id.mto_pull_id.route_id or line.order_id.warehouse_id.mto_pull_id.route_id
            if not mto_route:
                try:
                    mto_route = self.env['stock.warehouse']._find_global_route('stock.route_warehouse0_mto', _('Make To Order'))
                except UserError:
                    # if route MTO not found in ir_model_data, we treat the product as in MTS
                    pass

            if mto_route and mto_route in product_routes:
                line.is_mto = True
            else:
                line.is_mto = False


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    def group_by_best_crum(self):
        for record in self:
            crums = record.order_line.mapped("line_warehouse_id")
            mayor = 0
            crum_mayor = None
            for crum in crums:
                tam = len(record.order_line.filtered(lambda x: x.line_warehouse_id == crum))
                if tam > mayor:
                    mayor = tam
                    crum_mayor = crum
            if crum_mayor:
                acambiar = record.order_line.filtered(lambda x: x.line_warehouse_id != crum_mayor and x.line_warehouse_id)
                acambiar.line_warehouse_id = crum_mayor.id
                record.warehouse_id = crum_mayor.id

    @api.model_create_multi
    def create(self, vals):
        res = super(SaleOrder, self).create(vals)
        if self.env["ir.config_parameter"].sudo().get_param("nearest.default_crum_is_activate"):
            res.group_by_best_crum()
        return res
